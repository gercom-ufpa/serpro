#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info, debug
from mininet.node import Host, RemoteController
from mininet.term import makeTerm
from functools import partial

class VLANHost( Host ):
    "Host connected to VLAN interface"

    def config( self, vlan=100, **params ):
        """Configure VLANHost according to (optional) parameters:
           vlan: VLAN ID for default interface"""

        r = super( VLANHost, self ).config( **params )

        intf = self.defaultIntf()
        # remove IP from default, "physical" interface
        self.cmd( 'ifconfig %s inet 0' % intf )
        # create VLAN interface
        self.cmd( 'vconfig add %s %d' % ( intf, vlan ) )
        # assign the host's IP to the VLAN interface
        self.cmd( 'ifconfig %s.%d inet %s' % ( intf, vlan, params['ip'] ) )
        # update the intf name and host's intf map
        newName = '%s.%d' % ( intf, vlan )
        # update the (Mininet) interface to refer to VLAN interface name
        intf.name = newName
        # add VLAN interface to host's name to intf map
        self.nameToIntf[ newName ] = intf

        return r

    
def start_network (ip='127.0.0.1'):

    net = Mininet(controller=RemoteController,autoSetMacs=True)
    net.addController(ip=ip)

    #c1 = net.addController( 'c1', controller=RemoteController, ip=ip1)
    #c2 = net.addController( 'c2', controller=RemoteController, ip=ip2)
    #c3 = net.addController( 'c3', controller=RemoteController, ip=ip3)

    # DC_A switches
    dca_s1 = net.addSwitch('dca_s1')
    dca_s2 = net.addSwitch('dca_s2')
    dca_s3 = net.addSwitch('dca_s3')
    dca_s4 = net.addSwitch('dca_s4')
    dca_s5 = net.addSwitch('dca_s5')

    # DC_B switches
    dcb_s6 = net.addSwitch('dcb_s6')
    dcb_s7 = net.addSwitch('dcb_s7')
    dcb_s8 = net.addSwitch('dcb_s8')
    dcb_s9 = net.addSwitch('dcb_s9')
    dcb_s10 = net.addSwitch('dcb_s10')



    # Wire up the data center switches in the DC_A
    net.addLink(dca_s1, dca_s3) 
    net.addLink(dca_s1, dca_s4) 
    net.addLink(dca_s2, dca_s3)
    net.addLink(dca_s2, dca_s4)
    net.addLink(dca_s1, dca_s5)
    net.addLink(dca_s2, dca_s5)

    # Wire up the data center switches in the DC_A
    net.addLink(dcb_s6, dcb_s8) 
    net.addLink(dcb_s6, dcb_s9) 
    net.addLink(dcb_s7, dcb_s8)
    net.addLink(dcb_s7, dcb_s9)
    net.addLink(dcb_s6, dcb_s10)
    net.addLink(dcb_s7, dcb_s10)

    ## 
    ## BACKBONE 
    ##


    # Backbone switches
    bb_s11 = net.addSwitch('bb_s11')
    bb_s12 = net.addSwitch('bb_s12')
    bb_s13 = net.addSwitch('bb_s13')
    bb_s14 = net.addSwitch('bb_s14')
    bb_s15 = net.addSwitch('bb_s15')

    # Wire up the switches in the topology
    net.addLink( bb_s11, bb_s12 )
    net.addLink( bb_s11, bb_s13 )
    net.addLink( bb_s11, bb_s15 )
    net.addLink( bb_s12, bb_s14 )
    net.addLink( bb_s12, bb_s15 )
    net.addLink( bb_s13, bb_s14 )
    net.addLink( bb_s13, bb_s15 )
    net.addLink( bb_s14, bb_s15 )

    net.addLink( bb_s13, dca_s5 )
    net.addLink( bb_s14, dcb_s10 )


    ## 
    ## HOSTS (DC SERVERS)
    ##

    # Data center switches we want to attach to our hosts
    dcSwitches = [dca_s3, dca_s4, dcb_s8, dcb_s9]
    vlan100=100
    vlan200=200	

    # VLAN 100, DCA 
    host = net.addHost('h1', cls=VLANHost, vlan=vlan100)
    net.addLink(dcSwitches[0], host)
    host = net.addHost('h2', cls=VLANHost, vlan=vlan200)
    net.addLink(dcSwitches[0], host)
    # VLAN 200, DCA 
    host = net.addHost('h3', cls=VLANHost, vlan=vlan100)
    net.addLink(dcSwitches[1], host)
    host = net.addHost('h4', cls=VLANHost, vlan=vlan200)
    net.addLink(dcSwitches[1], host)

    # VLAN 100, DCB
    host = net.addHost('h5', cls=VLANHost, vlan=vlan100)
    net.addLink(dcSwitches[2], host)
    host = net.addHost('h6', cls=VLANHost, vlan=vlan200)
    net.addLink(dcSwitches[2], host)
    # VLAN 200, DCA 
    host = net.addHost('h7', cls=VLANHost, vlan=vlan100)
    net.addLink(dcSwitches[3], host)
    host = net.addHost('h8', cls=VLANHost, vlan=vlan200)
    net.addLink(dcSwitches[3], host)

    #net.build()

    #c1.start()
    #c2.start()
    #c3.start()


    # Connect DC A's switches
    #dca_s1.start([c1])
    #dca_s2.start([c1])
    #dca_s3.start([c1])
    #dca_s4.start([c1])
    #dca_s5.start([c1])

    # Connect BB's switches
    #bb_s1.start([c2])
    #bb_s2.start([c2])
    #bb_s3.start([c2])
    #bb_s4.start([c2])
    #bb_s5.start([c2])

    # Connect DC B's switches
    #dcb_s1.start([c3])
    #dcb_s2.start([c3])
    #dcb_s3.start([c3])
    #dcb_s4.start([c3])
    #dcb_s5.start([c3])

    
    net.start()

    CLI(net)

    net.stop()



if __name__ == '__main__':
    #setLogLevel('debug')
    setLogLevel('info')

    # Controller's IP addresses
    start_network(ip='10.0.0.2')

    info("done\n")
